<?php
session_start();
include "version.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SQL Client</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
		
		<style>
			<?php
				include "styles/common.css";
				include "styles/light.css";
				include "styles/dark.css";
			?>
		</style>
	</head>

	<body class="dark-mode">
		<div id="dbSelect">
			
		</div>
		
		<div id="erdPanel">
			<div id="erdContainer">
				<canvas id="erd" style="width: 100%; height: 100%;"></canvas>
			</div>
		</div>
		
		<div id="queryPanel">
			<div style="width: 100%; height: calc(100% - 38px);" id="queryField"></div>
			<div style="float: right;" class="row">
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownTheme" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Themes
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownTheme">
						<a class="dropdown-item" href="javascript:setClientTheme('light-mode');">Light</a>
						<a class="dropdown-item" href="javascript:setClientTheme('dark-mode');">Dark</a>
					</div>
				</div>
				
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownSelect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						View table
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownSelect" id="selectMenu">
						
					</div>
				</div>
				
				<button class="btn btn-success" onclick="runQuery();">Run Query >></button>
			</div>
		</div>
		
		<div id="loginOverlay">
			<div class="dialog" id="loginDialog">
				<h1>Easy SQL Client<sup><?php echo EASYSQL_VERSION; ?></sup></h1>
				<div class="form-group">
					<label for="loginServer">Server:</label>
					<input type="text" id="loginServer" class="form-control" value="">
				</div>
				
				<div class="form-group">
					<label for="loginUsername">Username:</label>
					<input type="text" id="loginUsername" class="form-control">
				</div>
				
				<div class="form-group">
					<label for="loginPassword">Password:</label>
					<input type="password" id="loginPassword" class="form-control">
					<small id="loginStatus" class="form-text text-muted"></small>
				</div>
				
				<div class="form-group row">
					<div class="col-3">
						<button id="loginButton" class="btn btn-success" onclick="login();">Log in</button>
					</div>
				</div>
			</div>
		</div>

		<div id="queryOverlay">
			<div class="dialog" id="queryDialog">
				<h1>Query</h1>
				<div id="queryResult" style="width: 100%; height: calc(100% - 120px); overflow: auto;">
				</div>
				<button class="btn btn-success" id="queryOK" onclick="document.getElementById('queryOverlay').style.display = 'none';" disabled>OK</button>
			</div>
		</div>
		
		<script src="/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
		<script>
			<?php include "client.js"; ?>
		</script>
	</body>
</html>
