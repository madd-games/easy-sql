# EasySQL

EasySQL is a very simple Web-based MySQL client written in PHP and JavaScript. It gives you a view
of the available databases, a field to enter queries with syntax highlighting, and an Entity Relationship
Diagram (ERD) of the currently-selected database.

I originally threw this together during a university course, but I found it useful over the years,
so I made it publicly available.

## Installation

The `ace-builds` directory needs to be in the root of the website, but the other files can be placed in any
location. Then simply navigate to `index.php` and it will work.