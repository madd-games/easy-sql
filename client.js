function escapeHtml(unsafe) {
	return unsafe
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;")
		.replace(/"/g, "&quot;")
		.replace(/'/g, "&#039;");
}

var g_server;
var g_username;
var g_password;
var g_database;
var g_nextERD = 0;
var g_erdBackground = 'white';
var g_erdForeground = 'black'

var editor = ace.edit("queryField");
editor.setTheme("ace/theme/dreamweaver");
editor.setFontSize(17);
editor.getSession().setUseSoftTabs(false);
editor.getSession().setMode("ace/mode/sql");
editor.getSession().setNewLineMode("unix");
editor.getSession().setTabSize(8);
editor.resize();

window.onload = function() {
	const value = localStorage.getItem('EasySQL.Theme');
	if (value !== null) {
		setClientTheme(value);
	}
}

window.onresize = function() {
	refreshERD();
}

function setClientTheme(className) {
	document.body.className = className;

	if (className === 'light-mode') {
		g_erdBackground = 'white';
		g_erdForeground = 'black';

		editor.setTheme('ace/theme/dreamweaver');
	}
	else {
		g_erdBackground = 'black';
		g_erdForeground = 'white';

		editor.setTheme('ace/theme/ambiance');
	}

	localStorage.setItem('EasySQL.Theme', className);
	refreshERD();
}

function SQL_Query(server, username, password, database, sql)
{
	var form = new FormData();
	form.append('server', server);
	form.append('username', username);
	form.append('password', password);
	form.append('database', database);
	form.append('query', sql);
	
	this.onComplete = function()
	{
	};
	
	var req = new XMLHttpRequest();
	req.query = this;
	
	req.onreadystatechange = function()
	{
		if (this.readyState == 4)
		{
			if (this.status != 200)
			{
				this.query.success = false;
				this.query.status = "HTTP " + this.status;
				this.query.result = {"status": "HTTP " + this.status};
				this.query.onComplete();
			}
			else
			{
				this.query.result = JSON.parse(this.responseText);
				this.query.status = this.query.result.status;
				this.query.success = (this.query.status == "0 OK");
				this.query.onComplete();
			};
		};
	};
	
	this.req = req;
	this.form = form;
	this.run = function()
	{
		this.req.open("POST", "query.php", true);
		this.req.send(this.form);
	};
};

function login()
{
	var status = document.getElementById('loginStatus');
	var btnLogin = document.getElementById('loginButton');
	
	btnLogin.disabled = true;
	status.innerHTML = "Logging in...";
	
	var server = document.getElementById('loginServer').value;
	var username = document.getElementById('loginUsername').value;
	var password = document.getElementById('loginPassword').value;
	
	var query = new SQL_Query(server, username, password, '', 'SHOW DATABASES;');
	query.server = server;
	query.username = username;
	query.password = password;
	query.onComplete = function()
	{
		var status = document.getElementById('loginStatus');
		var btnLogin = document.getElementById('loginButton');
		
		if (!this.success)
		{
			var errmsg = this.result.error || "No extra information";
			status.innerHTML = "Failed to log in: " + this.status + ": " + errmsg;
			btnLogin.disabled = false;
		}
		else
		{
			g_server = this.server;
			g_username = this.username;
			g_password = this.password;
			g_database = '';
			
			var table = this.result.table;
			var str = '<a href="javascript:switchDB(\'\');" class="active" id="dbsel_">(No database)</a>';
			for (i=0; i<table.length; i++)
			{
				var dbname = table[i].Database;
				str += '<a href="javascript:switchDB(\'' + dbname + '\');" id="dbsel_' + dbname + '">' + dbname + '</a>';
			};
			
			document.getElementById('dbSelect').innerHTML = str;
			document.getElementById('loginOverlay').style.display = "none";
			refreshERD();
		};
	};
	query.run();
};

function formatTableValue(value)
{
	if (value === null)
	{
		return '<i>NULL</i>';
	}
	else
	{
		return "<code>" + escapeHtml("" + value) + "</code>";
	};
};

function runQuery()
{
	var status = document.getElementById('queryResult');
	var overlay = document.getElementById('queryOverlay');
	var button = document.getElementById('queryOK');
	
	status.innerHTML = "Running query...";
	overlay.style.display = "block";
	button.disabled = true;
	
	var query = new SQL_Query(g_server, g_username, g_password, g_database, editor.getValue());
	query.onComplete = function()
	{
		var status = document.getElementById('queryResult');
		var button = document.getElementById('queryOK');
		
		button.disabled = false;
		if (!this.success)
		{
			var msg = this.result.error || "Unknown error";
			status.innerHTML = "ERROR: " + this.status + ": " + msg;
		}
		else
		{
			if (this.result.rowsAffected !== null)
			{
				status.innerHTML = `Query successful, <strong>${this.result.rowsAffected}</strong> rows affected.`;
			}
			else if (this.result.table.length == 0)
			{
				status.innerHTML = "Query successful, empty table returned.";
			}
			else
			{
				var headers = this.result.headers;
				var table = this.result.table;
				
				var str = '<table class="table table-striped table-hover">';
				str += '<thead class="table-inverse">';
				str += '<tr>';
				
				var i;
				for (i=0; i<headers.length; i++)
				{
					str += '<th>' + headers[i] + '</th>';
				};
				
				str += '</tr>';
				str += '</thead>';
				str += '<tbody>';
				
				for (i=0; i<table.length; i++)
				{
					str += '<tr>';
					
					var j;
					for (j=0; j<headers.length; j++)
					{
						var value = table[i][headers[j]];
						str += '<td>' + formatTableValue(value) + '</td>';
					};
					
					str += '</tr>';
				};
				
				str += '</tbody>';
				str += '</table>';
				status.innerHTML = str;
			};
			
			refreshDBList();
		};
	};
	query.run();
};

function switchDB(target)
{
	var oldDB = document.getElementById('dbsel_' + g_database);
	if (oldDB) oldDB.className = "";
	
	document.getElementById('dbsel_' + target).className = "active";
	g_database = target;
	
	refreshERD();
};

function refreshDBList()
{
	var query = new SQL_Query(g_server, g_username, g_password, '', 'SHOW DATABASES;');
	query.onComplete = function()
	{
		if (this.success)
		{
			var table = this.result.table;
			var str = '<a href="javascript:switchDB(\'\');" id="dbsel_">(No database)</a>';
			for (i=0; i<table.length; i++)
			{
				var dbname = table[i].Database;
				str += '<a href="javascript:switchDB(\'' + dbname + '\');" id="dbsel_' + dbname + '">' + dbname + '</a>';
			};
			
			document.getElementById('dbSelect').innerHTML = str;
			document.getElementById('dbsel_' + g_database).className = "active";
		};
	};
	query.run();
	refreshERD();
};

function refreshERD()
{
	var text;
	if (g_database == '')
	{
		text = "No database selected";
	}
	else
	{
		text = "Loading ERD...";
		
		var query = new SQL_Query(
			g_server,
			g_username,
			g_password,
			"information_schema",
			`
			SELECT
				COLUMNS.TABLE_NAME,
				COLUMNS.IS_NULLABLE,
				COLUMNS.COLUMN_NAME,
				COLUMNS.COLUMN_KEY,
				COLUMNS.COLUMN_TYPE,
				MAX(KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME) AS REFERENCED_TABLE_NAME,
				MAX(KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME) AS REFERENCED_COLUMN_NAME
			FROM COLUMNS
			LEFT JOIN KEY_COLUMN_USAGE ON
				COLUMNS.TABLE_NAME = KEY_COLUMN_USAGE.TABLE_NAME AND
				COLUMNS.COLUMN_NAME = KEY_COLUMN_USAGE.COLUMN_NAME
			WHERE COLUMNS.TABLE_SCHEMA='${g_database}'
			GROUP BY
				COLUMNS.TABLE_NAME,
				COLUMNS.IS_NULLABLE,
				COLUMNS.COLUMN_NAME,
				COLUMNS.COLUMN_KEY,
				COLUMNS.COLUMN_TYPE
			`
		);

		query.erdno = ++g_nextERD;
		query.dbname = g_database;
		query.onComplete = function()
		{
			var ERD_TABLE_WIDTH = 300;
			var ERD_CELL_HEIGHT = 20;
			var ERD_TABLE_SPACING = 100;
			var ERD_COL_WIDTH = ERD_TABLE_WIDTH / 3;
			var ERD_ARROW_SIZE = 20;
			
			if (this.erdno == g_nextERD)
			{
				var canvas = document.getElementById('erd');
				var ctx = canvas.getContext('2d');
				ctx.strokeStyle = g_erdForeground;
				ctx.fillStyle = g_erdBackground;
				ctx.fillRect(0, 0, canvas.width, canvas.height);
				
				if (this.success)
				{
					// first group the results by table
					var result = this.result.table;
					var columns = {};
					var tables = [];
					
					var i;
					for (i=0; i<result.length; i++)
					{
						columns[result[i].TABLE_NAME] = [];
						
						if (tables.indexOf(result[i].TABLE_NAME) == -1)
						{
							tables.push(result[i].TABLE_NAME);
						};
					};
					
					for (i=0; i<result.length; i++)
					{
						columns[result[i].TABLE_NAME].push(result[i]);
					};
					
					// create the select menu
					var menu = document.getElementById('selectMenu');
					var menustr = "";
					for (i=0; i<tables.length; i++)
					{
						menustr += '<a class="dropdown-item" href="javascript:selectTable(\'' + tables[i] + '\');">' + tables[i] + '</a>';
					};
					menu.innerHTML = menustr;
					
					// lay out the tables
					var putX = ERD_TABLE_SPACING;
					var putY = 100;
					var lowestY = 0;
					var maxPutX = canvas.width - ERD_TABLE_WIDTH - 10;
					var currentCol = 0;
					
					for (i=0; i<tables.length; i++)
					{
						var name = tables[i];
						var height = ERD_CELL_HEIGHT * (columns[tables[i]].length  + 1);
						
						columns[tables[i]].x = putX;
						columns[tables[i]].y = putY;
						columns[tables[i]].erdcol = currentCol;
						
						var bottom = putY + height;
						if (bottom > lowestY) lowestY = bottom;
						
						putX += ERD_TABLE_WIDTH + ERD_TABLE_SPACING;
						if ((++currentCol) == 2)
						{
							putX = ERD_TABLE_SPACING;
							putY = lowestY + ERD_TABLE_SPACING;
							currentCol = 0;
						}
						else
						{
							putY += 5;
						};
					};
					
					// draw the diagrams
					var wantHeight = lowestY + ERD_TABLE_SPACING;
					canvas.style.height = wantHeight + "px";
					canvas.height = wantHeight;
					
					ctx = canvas.getContext('2d');
					ctx.strokeStyle =g_erdForeground;
					ctx.fillStyle = g_erdBackground;
					ctx.fillRect(0, 0, canvas.width, canvas.height);
					ctx.fillStyle = g_erdForeground;
					ctx.font = "bold 30px sans-serif";
					ctx.textAlign = "center";
					ctx.fillText(query.dbname, canvas.width/2, 50);
					
					var usedPivots = [];
					for (i=0; i<tables.length; i++)
					{
						var plotX = columns[tables[i]].x;
						var plotY = columns[tables[i]].y;
						
						ctx.font = "12px sans-serif";
						
						ctx.beginPath();
						ctx.moveTo(plotX, plotY);
						ctx.lineTo(plotX+ERD_TABLE_WIDTH, plotY);
						ctx.lineTo(plotX+ERD_TABLE_WIDTH, plotY+ERD_CELL_HEIGHT);
						ctx.lineTo(plotX, plotY+ERD_CELL_HEIGHT);
						ctx.lineTo(plotX, plotY);
						ctx.stroke();
						ctx.fillText(tables[i], plotX+ERD_TABLE_WIDTH/2,
									plotY+ERD_CELL_HEIGHT/2+1);
						
						
						var nextY = plotY + ERD_CELL_HEIGHT;
						var j;
						
						for (j=0; j<columns[tables[i]].length; j++)
						{
							ctx.font = "12px sans-serif";
							var col = columns[tables[i]][j];
							
							var underline = false;
							if (col.COLUMN_KEY == "PRI")
							{
								underline = true;
								var width = ctx.measureText("PK").width;
								ctx.fillText("PK",
									plotX + ERD_COL_WIDTH/2 - 20,
									nextY + ERD_CELL_HEIGHT/2+1);
								ctx.beginPath();
								ctx.moveTo(
									plotX + ERD_COL_WIDTH/2 - width/2 - 20,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.lineTo(
									plotX + ERD_COL_WIDTH/2 + width/2 - 20,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.stroke();
							}
							
							if (col.REFERENCED_TABLE_NAME != null && col.REFERENCED_COLUMN_NAME != null)
							{
								ctx.font = "bold 12px sans-serif";
								ctx.fillText(
									"FK",
									plotX + ERD_COL_WIDTH/2 + 20,
									nextY + ERD_CELL_HEIGHT/2+1
								);
							}

							var width = ctx.measureText(col.COLUMN_NAME).width;
							ctx.fillText(col.COLUMN_NAME,
								plotX + ERD_COL_WIDTH + ERD_COL_WIDTH/2,
								nextY + ERD_CELL_HEIGHT/2+1);
						
							if (underline)
							{
								ctx.beginPath();
								ctx.moveTo(
									plotX + ERD_COL_WIDTH + ERD_COL_WIDTH/2 - width/2,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.lineTo(
									plotX + ERD_COL_WIDTH + ERD_COL_WIDTH/2 + width/2,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.stroke();
							};

							width = ctx.measureText(col.COLUMN_TYPE).width;
							ctx.fillText(col.COLUMN_TYPE,
								plotX + ERD_COL_WIDTH*2 + ERD_COL_WIDTH/2,
								nextY + ERD_CELL_HEIGHT/2+1);
							
							if (underline)
							{
								ctx.beginPath();
								ctx.moveTo(
									plotX + ERD_COL_WIDTH*2 + ERD_COL_WIDTH/2 - width/2,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.lineTo(
									plotX + ERD_COL_WIDTH*2 + ERD_COL_WIDTH/2 + width/2,
									nextY + ERD_CELL_HEIGHT/2 + 2);
								ctx.stroke();
							};
							
							if (col.REFERENCED_TABLE_NAME != null && col.REFERENCED_COLUMN_NAME != null)
							{
								// find where the target table is located
								var targetX = columns[col.REFERENCED_TABLE_NAME].x;
								var targetY = columns[col.REFERENCED_TABLE_NAME].y + ERD_CELL_HEIGHT/2;
								var targetNullable = "YES";
								
								// vector pointing at the target (in X direction)
								var towardsTarget = ERD_ARROW_SIZE;
								
								// ... and at the source
								var towardsSrc = ERD_ARROW_SIZE;
								
								// put the target at the correct row
								var k;
								for (k=0; k<columns[col.REFERENCED_TABLE_NAME].length; k++)
								{
									var ent = columns[col.REFERENCED_TABLE_NAME][k];
	
									if (ent.COLUMN_NAME == col.REFERENCED_COLUMN_NAME)
									{
										targetY += ERD_CELL_HEIGHT * (k+1);
										targetNullable = ent.IS_NULLABLE;
										break;
									};
								};
								
								// and from where we draw
								var srcX = plotX;
								var srcY = nextY + ERD_CELL_HEIGHT/2;
								
								// adjust to shortest path
								if (targetX < srcX)
								{
									targetX += ERD_TABLE_WIDTH;
									towardsTarget *= -1;
								};
								
								if (targetX > srcX)
								{
									srcX += ERD_TABLE_WIDTH;
									towardsSrc *= -1;
								};
								
								if (targetX == srcX)
								{
									if (columns[tables[i]].erdcol == 1)
									{
										targetX += ERD_TABLE_WIDTH;
										srcX += ERD_TABLE_WIDTH;
										
										towardsSrc *= -1;
										towardsTarget *= -1;
									};
								};
								
								// draw the line
								var pivotX = targetX - towardsTarget;
								while (usedPivots.indexOf(pivotX) != -1)
								{
									pivotX -= Math.floor(towardsTarget * 10 / ERD_ARROW_SIZE);
								};
								usedPivots.push(pivotX);
								
								ctx.strokeStyle = g_erdForeground;
								ctx.beginPath();
								ctx.moveTo(srcX, srcY);
								ctx.lineTo(srcX-towardsSrc, srcY);
								ctx.lineTo(pivotX, srcY);
								ctx.lineTo(pivotX, targetY);
								ctx.lineTo(targetX, targetY);
								ctx.stroke();
								
								// foreign key (source) = many, so crow's foot
								ctx.beginPath();
								ctx.moveTo(srcX-(towardsSrc/2), srcY);
								ctx.lineTo(srcX, srcY - (towardsSrc/3));
								ctx.moveTo(srcX-(towardsSrc/2), srcY);
								ctx.lineTo(srcX, srcY + (towardsSrc/3));
								ctx.stroke();
								
								// draw either the "mandator" or "optional" thing on
								// the crow's foot, depending on whether the foreign key
								// can be NULL.
								ctx.beginPath();
								if (col.IS_NULLABLE == "NO")
								{
									ctx.moveTo(srcX-(towardsSrc*3/4), srcY - ERD_CELL_HEIGHT/3);
									ctx.lineTo(srcX-(towardsSrc*3/4), srcY + ERD_CELL_HEIGHT/3);
								}
								else
								{
									ctx.arc(srcX-(towardsSrc*4/5), srcY,
										ERD_CELL_HEIGHT / 4,
										0, 2*Math.PI);
								};
								ctx.stroke();
								
								// the referenced value = one, so the funny line
								ctx.beginPath();
								ctx.moveTo(targetX-(towardsTarget/2), targetY - ERD_CELL_HEIGHT/3);
								ctx.lineTo(targetX-(towardsTarget/2), targetY + ERD_CELL_HEIGHT/3);
								ctx.stroke();
								
								// now the optional/mandatory thing
								ctx.beginPath();
								if (targetNullable == "NO")
								{
									ctx.moveTo(targetX-(towardsTarget*3/4), targetY - ERD_CELL_HEIGHT/3);
									ctx.lineTo(targetX-(towardsTarget*3/4), targetY + ERD_CELL_HEIGHT/3);
								}
								else
								{
									ctx.arc(targetX-(towardsTarget*4/5), targetY,
											ERD_CELL_HEIGHT / 4,
											0, 2 * Math.PI);
								};
								ctx.stroke();
							};
							
							nextY += ERD_CELL_HEIGHT;
						};
						
						ctx.beginPath();
						ctx.moveTo(plotX, plotY + ERD_CELL_HEIGHT);
						ctx.lineTo(plotX, nextY);
						ctx.lineTo(plotX + ERD_TABLE_WIDTH, nextY);
						ctx.lineTo(plotX + ERD_TABLE_WIDTH, plotY + ERD_CELL_HEIGHT);
						ctx.stroke();
						
						ctx.beginPath();
						ctx.moveTo(plotX + ERD_COL_WIDTH, plotY + ERD_CELL_HEIGHT);
						ctx.lineTo(plotX + ERD_COL_WIDTH, nextY);
						ctx.stroke();
						
						ctx.beginPath();
						ctx.moveTo(plotX + ERD_COL_WIDTH*2, plotY + ERD_CELL_HEIGHT);
						ctx.lineTo(plotX + ERD_COL_WIDTH*2, nextY);
						ctx.stroke();
					};
				}
				else
				{
					ctx.fillStyle = g_erdForeground;
					ctx.font = "bold 30px sans-serif";
					ctx.textAlign = "center";
					ctx.fillText("(Query failed)", canvas.width/2, 50);
				};
			};
		};
		query.run();
		
	};
	
	var canvas = document.getElementById('erd');
	canvas.width = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;
	
	var ctx = canvas.getContext('2d');
	ctx.fillStyle = g_erdBackground;
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	
	ctx.fillStyle = g_erdForeground;
	ctx.font = "bold 30px sans-serif";
	ctx.textAlign = "center";
	ctx.fillText(text, canvas.width/2, 50);
};

function selectTable(name)
{
	editor.setValue("SELECT * FROM `" + name + "`;");
};