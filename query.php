<?php
/*
	Madd Easy SQL Client

	Copyright (c) 2014-2017, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * query.php
 * A simple script for executing MySQL queries, and returning results in JSON, for use by JavaScript.
 * This entire script is self-contained. The JavaScript library builds on top of it.
 *
 * This script accepts the following fields via POST:
 *	server
 *		MySQL host name.
 *
 *	username
 *		User name on the MySQL server.
 *
 *	password
 *		The user's password.
 *
 *	database (OPTIONAL)
 *		The database to select. If not set (or empty), no database is selected.
 *
 *	query (OPTIONAL)
 *		MySQL query to execute. If not set (or empty), the connection is attempted but no query is
 *		executed, and an empty table is returned.
 *
 * The returned JSON is a dictionar containing a "status" key, whose values is a string, which is "0 OK" on
 * success, or an error number and message on error. The error will be from within the script. For SQL errors,
 * an additional "error" key containing a full message is given. On success (when status is "0 OK"), a "table"
 * key will be present, containing an array of tuples. A tuple is a dictionary which maps an attribute name to
 * its value.
 */

header('Content-Type: application/json');

if (isset($_POST['server'])) {
	$server = $_POST['server'];
} else {
	echo '{"status": "2 Missing parameter server"}';
	exit;
}

if (isset($_POST['database']) && $_POST['database'] != '') {
	$connstr = 'mysql:host=' . $server . ';dbname=' . $_POST['database'];
} else {
	$connstr = 'mysql:host=' . $server;
}

if (isset($_POST['username'])) {
	$username = $_POST['username'];
} else {
	echo '{"status": "2 Missing parameter username"}';
	exit;
}

if (isset($_POST['password'])) {
	$password = $_POST['password'];
} else {
	echo '{"status": "2 Missing parameter password"}';
	exit;
}

if (isset($_POST['query'])) {
	$query = $_POST['query'];
} else {
	$query = '';
}

try {
	$pdo = new PDO($connstr, $username, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
} catch (PDOException $ex) {
	echo json_encode(array("status" => "3 Connection failed", "error" => $ex->getMessage()));
	exit;
}

if ($query == '') {
	echo '{"status": "0 OK", "table": []}';
	exit;
}

try {
	$st = $pdo->query($query);
} catch (PDOException $ex) {
	echo json_encode(array("status" => "4 Query failed", "error" => $ex->getMessage()));
	exit;
}

$rowsAffected = null;
if ($st->columnCount()) {
	try {
		$table = $st->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $ex) {
		echo json_encode(array("status" => "5 Result fetch failed", "error" => $ex->getMessage()));
		exit;
	}
	;
} else {
	$table = array();
	$rowsAffected = $st->rowCount();
}

if (!$table) {
	$headers = array();
} else {
	$headers = array_keys($table[0]);
}

$data = array(
	"status" => "0 OK",
	"table" => $table,
	"headers" => $headers,
	"rowsAffected" => $rowsAffected
);

$json = json_encode($data, JSON_INVALID_UTF8_SUBSTITUTE);
if ($json === false) {
	echo json_encode(array("status" => "6 Encode failed", "error" => "Could not encode the data as JSON."));
	exit;
}

echo $json;
