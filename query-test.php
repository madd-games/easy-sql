<?php
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Query Test</title>
	</head>
	
	<body>
		<h1>Query Test</h1>
		
		<form action="query.php" method="post">
			Server: <input type="text" name="server" value="localhost"><br />
			Username: <input type="text" name="username"><br />
			Password: <input type="password" name="password"><br />
			Database (optional): <input type="text" name="database"><br />
			Query:<br />
			<textarea name="query" style="width: 100%; height: 500px;"></textarea><br />
			<input type="submit" value="Execute">
		</form>
	</body>
</html>
