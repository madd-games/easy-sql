<?php
/*
	Madd Easy SQL Client

	Copyright (c) 2014-2017, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * dump.php
 * Make an SQL dump.
 *
 * ALPHA STATE. DON'T TRUST THIS SCRIPT TOO MUCH, IT MIGHT BE MISSING DATA!
 *
 * Variables "server", "username", "password" and "database" are to be passed over POST.
 *
 * On error, this script prints a message with the format "--ERROR: ...". Otherwise, it prints
 * SQL statements for recreating the database, preceded by a signature comment.
 */

header('Content-Type: text/plain');

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
	echo '--ERROR: 1 Insecure connection';
	exit();
}

if (isset($_POST['server'])) {
	$server = $_POST['server'];
} else {
	echo '--ERROR: 2 Missing parameter server';
	exit;
}

if (isset($_POST['database']) && $_POST['database'] != '') {
	$connstr = 'mysql:host=' . $server . ';dbname=' . $_POST['database'];
} else {
	echo '--ERROR: 2 Missing parameter database';
	exit;
}

if (isset($_POST['username'])) {
	$username = $_POST['username'];
} else {
	echo '--ERROR: 2 Missing parameter username';
	exit;
}

if (isset($_POST['password'])) {
	$password = $_POST['password'];
} else {
	echo '--ERROR: 2 Missing parameter password';
	exit;
}

try {
	$pdo = new PDO($connstr, $username, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
} catch (PDOException $ex) {
	echo '--ERROR: ' . $ex->getMessage();
	exit;
}

echo '-- SQL dump for ' . $_POST['database'] . ' produced by Madd Easy SQL on ' . date(DATE_RSS) . "\n";
echo "\n";

$st = $pdo->query("SHOW TABLES;");
$tables = $st->fetchAll(PDO::FETCH_BOTH);

foreach ($tables as $tab) {
	$st = $pdo->query("SHOW CREATE TABLE `" . $tab[0] . "`;");
	$create = $st->fetchAll(PDO::FETCH_ASSOC)[0]["Create Table"];

	if (substr($create, 0, 12) === "CREATE TABLE") {
		$kind = "TABLE";
	} else {
		$kind = "VIEW";
	}

	echo "DROP $kind IF EXISTS `$tab[0]`;\n";
	echo "$create;\n";

	// dump all the data
	if ($kind == "TABLE") {
		echo "\n";

		$st = $pdo->query("SELECT * FROM `$tab[0]`;");
		$rows = $st->fetchAll(PDO::FETCH_ASSOC);

		foreach ($rows as $row) {
			$inslist = "";
			$vallist = "";

			foreach ($row as $key => $value) {
				$inslist = $inslist . ", `" . $key . "`";
				$vallist = $vallist . ", " . $pdo->quote($value);
			}

			// remove the initial ", " from each
			if ($inslist)
				$inslist = substr($inslist, 2);
			if ($vallist)
				$vallist = substr($vallist, 2);

			echo "INSERT INTO `$tab[0]`($inslist) VALUES ($vallist);\n";
		}
	}

	echo "\n";
}